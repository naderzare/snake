import zmq
import time
import random
import pygame
import threading
import sys
from vec2d import *


class snake:
    def __init__(self, _id):
        self.id = _id
        self.head = vec2d(0, 0)
        self.end = vec2d(0,0)
        self.body = []
        self.eval = 0
        if self.id == 0:
            self.head.set(3, 1)
            self.body.append(self.head)
            self.body.append(vec2d(2, 1))
            self.body.append(vec2d(1, 1))
            self.end = self.body[-1]
        elif self.id == 1:
            self.head.set(46, 1)
            self.body.append(self.head)
            self.body.append(vec2d(47, 1))
            self.body.append(vec2d(48, 1))
            self.end = self.body[-1]
        elif self.id == 2:
            self.head.set(46, 48)
            self.body.append(self.head)
            self.body.append(vec2d(47, 48))
            self.body.append(vec2d(48, 48))
            self.end = self.body[-1]
        elif self.id == 3:
            self.head.set(3, 48)
            self.body.append(self.head)
            self.body.append(vec2d(2, 48))
            self.body.append(vec2d(1, 48))
            self.end = self.body[-1]
        self.size = len(self.body)
    def reset(self):
        self.head = vec2d(0, 0)
        self.end = vec2d(0, 0)
        self.body = []
        self.eval -= 1
        if self.id == 0:
            self.head.set(3, 1)
            self.body.append(self.head)
            self.body.append(vec2d(2, 1))
            self.body.append(vec2d(1, 1))
            self.end = self.body[-1]
        elif self.id == 1:
            self.head.set(46, 1)
            self.body.append(self.head)
            self.body.append(vec2d(47, 1))
            self.body.append(vec2d(48, 1))
            self.end = self.body[-1]
        elif self.id == 2:
            self.head.set(46, 48)
            self.body.append(self.head)
            self.body.append(vec2d(47, 48))
            self.body.append(vec2d(48, 48))
            self.end = self.body[-1]
        elif self.id == 3:
            self.head.set(3, 48)
            self.body.append(self.head)
            self.body.append(vec2d(2, 48))
            self.body.append(vec2d(1, 48))
            self.end = self.body[-1]
        self.size = len(self.body)

def fix_pos(newhead):
    if newhead.x == 50:
        newhead.x = 0
    elif newhead.y == 50:
        newhead.y = 0
    elif newhead.y == -1:
        newhead.y = 49
    elif newhead.x == -1:
        newhead.x = 49
    return newhead


class game:
    def __init__(self):
        self.time = 0
        self.maxtime = 1000
        self.snakes = [snake(0), snake(1), snake(2), snake(3)]
        self.walls = [vec2d(8,1)]
        self.lastactions = [vec2d(1, 0), vec2d(-1, 0), vec2d(-1, 0), vec2d(1, 0)]

        self.board = []
        for i in range(0,50):
            b = []
            for j in range(0,50):
                b.append(-1)
            self.board.append(b)
        for s in self.snakes:
            for b in s.body:
                self.board[b.x][b.y] = s.id
        for w in self.walls:
            self.board[w.x][w.y] = -2
        self.apple = vec2d(random.randrange(0, 50), random.randrange(0, 50))
        self.board[self.apple.x][self.apple.y] = -3
        self.context = zmq.Context()
        self.sender = self.context.socket(zmq.PUB)
        self.sender.bind("tcp://*:5657")
        self.receiver = self.context.socket(zmq.PULL)
        self.receiver.bind("tcp://*:5658")
        self.sender_id = self.context.socket(zmq.REP)
        self.sender_id.bind("tcp://*:5659")
        self.password = ['a','b','c','d']
        self.is_online = True
        self.start_time_cycle = time.time()

    def set_new_apple(self):
        if self.board[self.apple.x][self.apple.y] == -3:
            self.board[self.apple.x][self.apple.y] = -1
        self.apple = vec2d(random.randrange(0, 50), random.randrange(0, 50))
        self.board[self.apple.x][self.apple.y] = -3

    def update(self):
        snakes_status = {}
        for s in self.snakes:
            newhead = fix_pos(s.head + self.lastactions[s.id])
            snake_status = {'id':s.id,'body':s.body,'newhead':newhead,'eat':False,'head2head':False,'gowall':False,'gosnake':False}
            if newhead == self.apple:
                snake_status['eat'] = True
            if newhead in self.walls:
                snake_status['gowall'] = True
            snakes_status[s.id]=snake_status
        for s1s in snakes_status:
            for s2s in snakes_status:
                if snakes_status[s1s]['id'] == snakes_status[s2s]['id']:
                    continue
                if snakes_status[s1s]['newhead'] == snakes_status[s2s]['newhead']:
                    snakes_status[s1s]['head2head'] = True
                    snakes_status[s2s]['head2head'] = True
        for s1s in snakes_status:
            for s2s in snakes_status:
                if snakes_status[s2s]['eat']:
                    if snakes_status[s1s]['newhead'] in snakes_status[s2s]['body']:
                        snakes_status[s1s]['gosnake'] = True
                else:
                    if snakes_status[s1s]['newhead'] in snakes_status[s2s]['body'][:-1]:
                        snakes_status[s1s]['gosnake'] = True

        for s in self.snakes:
            newhead = fix_pos(s.head + self.lastactions[s.id])
            snake_status = snakes_status[s.id]
            if snake_status['head2head'] or snake_status['gowall'] or snake_status['gosnake']:
                for b in s.body:
                    self.board[b.x][b.y] = -1
                s.reset()
                for b in s.body:
                    self.board[b.x][b.y] = s.id
            elif snake_status['eat']:
                self.board[newhead.x][newhead.y] = s.id
                s.head = newhead
                s.body.insert(0, newhead)
                s.eval += 1
                self.apple = vec2d(random.randrange(0, 50), random.randrange(0, 50))
                self.board[self.apple.x][self.apple.y] = -3
            else:
                self.board[s.end.x][s.end.y] = -1
                self.board[newhead.x][newhead.y] = s.id
                s.head = newhead
                s.body.insert(0, newhead)
                del s.body[-1]
                s.end = s.body[-1]
        self.time += 1
        if self.time == self.maxtime:
            self.is_online = False
    def send_state(self):
        while self.start_time_cycle > time.time() - 0.2:
            time.sleep(0.01)
        s = []
        for y in range(50):
            for x in range(50):
                s.append(str(self.board[x][y])+',')
        h = []
        for sn in self.snakes:
            h.append(str(sn.head.x) + ',')
            h.append(str(sn.head.y) + ',')
        state = 'state:'+''.join(s)
        head = 'head:'+''.join(h)
        self.sender.send_string(state + head)
        self.start_time_cycle = time.time()
        if not self.is_online:
            self.sender.send_string('finish')
    def set_action(self,ac):
        id = int(ac[0])
        passw = ac[1]
        if not passw == self.password[id]:
            return
        lastact = ac[2]
        lastaction = vec2d(0, 0)
        if lastact == 'r':
            lastaction = vec2d(1, 0)
        elif lastact == 'l':
            lastaction = vec2d(-1, 0)
        elif lastact == 'u':
            lastaction = vec2d(0, 1)
        elif lastact == 'd':
            lastaction = vec2d(0, -1)
        self.lastactions[id] = lastaction
    def rec_action(self):
        while self.start_time_cycle > time.time() - 0.7:
            ac = self.receiver.recv_multipart()
            id = int(ac[0])
            passw = ac[1]
            if not passw == self.password[id]:
                continue
            lastact = ac[2]
            lastaction = vec2d(0, 0)
            if lastact == 'r':
                lastaction = vec2d(1, 0)
            elif lastact == 'l':
                lastaction = vec2d(-1, 0)
            elif lastact == 'u':
                lastaction = vec2d(0, 1)
            elif lastact == 'd':
                lastaction = vec2d(0, -1)
            self.lastactions[id] = lastaction
        self.update()
        self.send_state()
    def printgame(self):
        import os
        clear = lambda: os.system('clear')
        clear()

        board = []
        for j in range(50):
            b = []
            for i in range(50):
                b.append('')
            board.append(b)
        for j in range(50):
            for i in range(50):
                if self.board[i][j] >= 0:
                    board[j][i] = 's'
                elif self.board[i][j] == -1:
                    board[j][i] = ' '
                elif self.board[i][j] == -2:
                    board[j][i] = 'w'
                elif self.board[i][j] == -3:
                    board[j][i] = 'a'
        for j in range(50):
            print (''.join(board[j]))
    def send_id(self):
        num_s = 0
        while num_s < 4:
            print ('send id',num_s)
            h = self.sender_id.recv()
            if h == 'hi':
                self.sender_id.send(str(num_s)+','+self.password[num_s])
                resp = self.sender_id.recv()
                print ('rec', resp)
                if resp == 'ok':
                    self.sender_id.send(str(num_s))
                    num_s += 1
            time.sleep(0.3)
        self.sender_id.close()

def rec_action(g):
    while g.is_online:
        ac = g.receiver.recv_multipart()
        g.set_action(ac)
        time.sleep(0.01)


class graphic():
    def __init__(self):
        pygame.init()
        size = width, height = 400, 600
        self.color = {
            'black': (0, 0, 0),
            'blackh': (64, 64, 64),
            'white': (255, 255, 255),
            'gray': (220, 220, 220),
            'brown': (139, 69, 19),
            'green': (0, 250, 0),
            'blue': (0, 0, 250),
            'blueh': (124, 137, 255),
            'red': (255, 0, 255),
            'redh': (255, 102, 102),
            'yellow': (153, 153, 153),
            'yellowh': (255, 255, 51)
        }
        self.screen = pygame.display.set_mode(size)

    def snake_color(self, id):
        if id == 0:
            color = self.color['red']
        if id == 1:
            color = self.color['blue']
        if id == 2:
            color = self.color['black']
        if id == 3:
            color = self.color['yellow']
        return color

    def show(self,g):
        self.screen.fill(self.color['gray'])
        for i in range(50):
            for j in range(50):
                if g.board[i][j] == -1:
                    color = self.color['gray']
                if g.board[i][j] == -2:
                    color = self.color['brown']
                if g.board[i][j] == -3:
                    color = self.color['green']
                if g.board[i][j] == 0:
                    color = self.color['red']
                    if g.snakes[0].head == vec2d(i, j):
                        color = self.color['redh']
                if g.board[i][j] == 1:
                    color = self.color['blue']
                    if g.snakes[1].head == vec2d(i, j):
                        color = self.color['blueh']
                if g.board[i][j] == 2:
                    color = self.color['black']
                    if g.snakes[2].head == vec2d(i, j):
                        color = self.color['blackh']
                if g.board[i][j] == 3:
                    color = self.color['yellow']
                    if g.snakes[3].head == vec2d(i, j):
                        color = self.color['yellowh']
                if not g.board[i][j] == -1:
                    pygame.draw.rect(self.screen, color, [i * 8, j * 8 + 8, 8, 8])
        for s in g.snakes:
            text = 'snake'+str(s.id)+':'+str(s.eval)
            pygame.font.init()  # you have to call this at the start,
            myfont = pygame.font.SysFont('Arial', 15)
            textsurface = myfont.render(text, False, self.snake_color(s.id))
            self.screen.blit(textsurface, (s.id * 100, 500))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        pygame.display.flip()

def main():
    g = game()
    gr = graphic()
    rec_ac = threading.Thread(target=rec_action, args=[g, ])
    rec_ac.start()
    g.send_id()
    g.send_state()
    while(g.is_online):
        g.update()
        g.send_state()
        g.printgame()
        gr.show(g)
    rec_ac.join()
main()
