
class vec2d:
    def __init__(self, _x=0, _y=0):
        self.x = _x
        self.y = _y

    def setx(self, _x):
        self.x = _x

    def sety(self, _y):
        self.y = _y

    def set(self, _x, _y):
        self.x = _x
        self.y = _y

    def __add__(self, another):
        return vec2d(self.x + another.x, self.y + another.y)

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False

    def __str__(self):
        return "("+str(self.x)+","+str(self.y)+")"

    def dist(self, another):
        dx = abs(self.x - another.x)
        dy = abs(self.y - another.y)
        return dx + dy
