import zmq
from vec2d import *
context = zmq.Context()
receiver = context.socket(zmq.SUB)
receiver.connect("tcp://localhost:5657")
sender = context.socket(zmq.PUSH)
sender.connect("tcp://localhost:5658")
receiver_id = context.socket(zmq.REQ)
receiver_id.connect("tcp://localhost:5659")
zip_filter = "state"
if isinstance(zip_filter, bytes):
    zip_filter = zip_filter.decode('ascii')
receiver.setsockopt_string(zmq.SUBSCRIBE, zip_filter)


receiver_id.send('hi')
id_pass = receiver_id.recv().split(',')
print 'rec', id_pass
id = int(id_pass[0])
password = id_pass[1]
receiver_id.send('ok')
ok = receiver_id.recv()
print ok
receiver_id.close()

board = []
showboard = []
for j in range(50):
    b = []
    for i in range(50):
        b.append(0)
    showboard.append(b)
    board.append(b)


def set_show_board(board):
    global showboard
    for j in range(50):
        for i in range(50):
            if board[j][i] >= 0:
                showboard[j][i] = 's'
            elif board[j][i] == -1:
                showboard[j][i] = ' '
            elif board[j][i] == -2:
                showboard[j][i] = 'w'
            elif board[j][i] == -3:
                showboard[j][i] = 'a'


def show_board():
    global showboard
    import os
    clear = lambda: os.system('clear')
    clear()

    for j in range(50):
        print ''.join(showboard[j])


def parse_msg(msg):
    head = []
    head.append(vec2d(0, 0))
    head.append(vec2d(0, 0))
    head.append(vec2d(0, 0))
    head.append(vec2d(0, 0))
    if 'state' in msg:
        f_head = msg.find('head:')
        f_state = msg.find('state:')
        heads = msg[f_head+5:].split(',')
        state = msg[f_state + 6:f_head].split(',')
        apple = vec2d(0,0)
        walls = []
        snakes = []
        r = 0
        for j in range(50):
            for i in range(50):
                board[i][j] = int(state[r])
                if board[i][j] == -3:
                    apple = vec2d(i,j)
                if board[i][j] == -2:
                    walls.append(vec2d(i,j))
                if board[i][j] >=0:
                    snakes.append(vec2d(i,j))
                r += 1
        for i in range(4):
            head[i] = vec2d(int(heads[i*2]), int(heads[i*2+1]))
        isOnline = True
        return isOnline,head,apple,walls,snakes
    return False,head,vec2d(0,0),[],[]

while True:
    msg = receiver.recv_string()
    isOnline,head,apple,walls,snakes = parse_msg(msg)
    if isOnline:
        x = str(id)
        target = []
        myhead = head[id]
        target.append(myhead + vec2d(1, 0))
        target.append(myhead + vec2d(0, 1))
        target.append(myhead + vec2d(-1, 0))
        target.append(myhead + vec2d(0, -1))
        mindist = 1000
        besttarget = 0
        if id == 0:
            safe = []
            for t in range(4):
                if (not target[t] in walls) and (not target[t] in snakes):
                    sa = {'num': t, 'vec': target[t]}
                    safe.append(sa)
            for t in safe:
                if t['vec'].dist(apple) < mindist:
                    mindist = t['vec'].dist(apple)
                    besttarget = t['num']
        else:
            for t in range(4):
                if target[t].dist(apple) < mindist:
                    mindist = target[t].dist(apple)
                    besttarget = t
        ac = "r"
        if besttarget == 1:
            ac = "u"
        elif besttarget == 2:
            ac = "l"
        elif besttarget == 3:
            ac = "d"
        sender.send_multipart([x,password, ac])
        print 'send'
